#!/usr/bin/env groovy

properties([gitLabConnection(gitLabConnection: '', jobCredentialId: ''), parameters([choice(choices: ['preprod', 'prod', 'loki'], description: 'Enter the Namespace to deploy', name: 'Namespace'), choice(choices: ['dss-rrs-FITQUINT', 'dss-rrs-bloombergquint', 'dss-rrs-quintype'], description: 'Provide the ClientName', name: 'ClientName'), string(description: 'Enter the ImageID to deploy', name: 'IMAGEID')])])
pipeline {
    environment {
        IMAGE_BUILD_TAG = "$BRANCH_NAME-$BUILD_NUMBER"
        NAMESPACE = "prod"
        APP_NAME = "dss-rrs"
        ENVIRONMENT1 = "$Namespace"
        IMAGE_TAG = "$IMAGEID"
        BRANCH_NAME1 = "$BRANCH"
  }
    agent {
        kubernetes {
            yaml '''
            apiVersion: v1 
            kind: Pod 
            metadata: 
              name: dind 
            spec: 
              containers: 
              - name: dind-daemon 
                image: docker:dind 
                resources: 
                  requests: 
                    cpu: 20m 
                    memory: 512Mi 
                securityContext: 
                  privileged: true 
                volumeMounts: 
                - name: docker-graph-storage 
                  mountPath: /var/lib/docker 
              volumes: 
              - name: docker-graph-storage 
                emptyDir: {}
            '''
        }
    }
    stages {
        stage('Git checkout') {
            steps {
                container('dind-daemon') {
                    script {
                        def vars = checkout scm
                        vars.each { k,v -> env.setProperty(k, v) }
                    
                    }                               
                }
            }
        }
        stage('deploy to k8s'){
          steps {
            container('dind-daemon'){
              script{    
                    sh 'apk update'
                    sh 'apk add curl'
                    sh 'curl -LO https://storage.googleapis.com/kubernetes-release/release/v1.18.0/bin/linux/amd64/kubectl'
                    sh 'chmod +x ./kubectl'
                    sh 'mv ./kubectl /usr/local/bin/kubectl'
                    sh 'kubectl version --client'
                    
                    withKubeConfig([credentialsId: 'kubefile', serverUrl: 'https://cb-69f63dae.hcp.centralindia.azmk8s.io:443']) {
                        sh 'kubectl get nodes'
                        sh 'cat ${ClientName}.yaml | sed -e "s/{{ENVIRONMENT}}/$ENVIRONMENT1/g" -e "s/{{IMAGE_TAG}}/$IMAGE_TAG/g" | tee ${ClientName}.yaml | kubectl apply -f -'  
                    }          
              }    

            }
          }
        }
    }
post {
    success {
      slackSend (color: '#00FF00', message: "SUCCESSFUL: NameSpace:'${env.ENVIRONMENT1}' Job:'${env.JOB_NAME}' BuildNo:'[${env.BUILD_NUMBER}]' BuildUrl:(${env.BUILD_URL})")

      hipchatSend (color: 'GREEN', notify: true,
          message: "SUCCESSFUL: Namespace Job '${env.ENVIRONMENT1} ${env.JOB_NAME} [${env.BUILD_NUMBER}]' (${env.BUILD_URL})"
        )
    }

    failure {
      slackSend (color: '#FF0000', message: "FAILED: NameSpace:'${env.ENVIRONMENT1}' Job:'${env.JOB_NAME}' BuildNo:'[${env.BUILD_NUMBER}]' BuildUrl:(${env.BUILD_URL})")

      hipchatSend (color: 'RED', notify: true,
          message: "FAILED: Namespace Job '${env.ENVIRONMENT1} ${env.JOB_NAME} [${env.BUILD_NUMBER}]' (${env.BUILD_URL})"
        )
    }
  }


}

def dockerBuildAndPushtoRegistry(
        String image,
        tags = [],
        dockerfile = 'Dockerfile',
        context = '.') {

  def dockerRoot = "registrycoffeebeans.azurecr.io/"
  def imageName = image.startsWith(dockerRoot) ? image : dockerRoot + image

  def buildArgs = tags.collect { "-t $imageName:$it" }.join(" ")

  sh "docker build -f $dockerfile $buildArgs $context"

  withCredentials([usernamePassword(credentialsId: 'acr-token', usernameVariable: 'username', passwordVariable: 'password')]) {
    sh 'docker login -u ${username} -p ${password} registrycoffeebeans.azurecr.io'
    tags.each {
      sh "docker push $imageName:$it"
    }
  }
}

